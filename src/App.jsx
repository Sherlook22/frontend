import './App.scss';
import 'materialize-css';
import React from 'react';
import routes from './router/routes';
import { RouteRender } from './helpers/RouteRender';
import { BrowserRouter as Router, Switch} from 'react-router-dom';
import NavBar from './components/shared/NavBar';

function App() {
  return (
    <div>
      <NavBar />
      <Router>
        <Switch>
          { routes.map((route, index) => 
            <RouteRender key={index} {...route}/>
          )}
        </Switch>
      </Router>
    </div>
  );
} 

export default App;
