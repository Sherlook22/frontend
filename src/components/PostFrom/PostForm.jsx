import React from "react";
import "./postform.scss";
import { httpPost, httpPatch } from '../../adapters/core/http'
import { useForm } from "react-hook-form";

export default function PostForm(props) {
    const { register, handleSubmit, errors } = useForm();
    const { modalContent, setReloadPosts, isVisible, setErrors } = props;

    const onSubmit = (data, e) => {
            if(modalContent.title === '' || modalContent.body === '') 
                createForm(data, e)
            else 
                editForm(data, e)
    };

    async function createForm(data, e){
        const resp = await httpPost('/posts', data);
        if(resp) {
            e.target.reset();
            setReloadPosts(true)        
            isVisible(false);
            setErrors({msj: "El post fue creado correctamente"});
            return 
        }
        setErrors({err: "La creacion del post fallo"})
    }
    async function editForm(data, e){
        const resp = await httpPatch(`/posts/${modalContent.id}`, data);
        if(resp) {
            e.target.reset();
            setReloadPosts(true)        
            isVisible(false);
            setErrors({msj: "El Post Fue actualizado correctamente"})
            return
        }
        setErrors({err: "La edicion del post fallo"})
    }

    return (
        <div className="row form">
            <form id="form-modal" className="col s12" onSubmit={handleSubmit(onSubmit)}>
                <div className="row">
                    <div className="input-field col s12">
                        <input
                            name="title"
                            type="text"
                            defaultValue={modalContent.title}
                            className={errors.title ? "invalid": ""}
                            ref={register({ required: true })}
                        />
                        <label htmlFor="title">Titulo</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                        <textarea
                            className={`materialize-textarea ${!errors.postContent ? "": 'invalid'}`}
                            name="postContent"
                            defaultValue={modalContent.body}
                            ref={register({ required: true })}
                        ></textarea>
                        <label htmlFor="post-content">Post Content</label>
                    </div>
                </div>
                <button
                    className="btn col s12 waves-effect waves-light"
                    type="submit"
                    name="action"
                >
                    Submit
                    <i className="material-icons right">send</i>
                </button>
            </form>
        </div>
    );
}
