import React from "react";
import './navbar.scss';
import { Icon, NavItem, Navbar } from 'react-materialize';

export default function NavBar() {
    return(
        <Navbar
            className="grey darken-3"
            alignLinks="right"
            brand={
                <a className="brand-logo" href="/">
                    PostApp
                </a>
            }
            id="mobile-nav"
            menuIcon={<Icon>menu</Icon>}
            options={{
                draggable: true,
                edge: "left",
                inDuration: 250,
                onCloseEnd: null,
                onCloseStart: null,
                onOpenEnd: null,
                onOpenStart: null,
                outDuration: 200,
                preventScrolling: true,
            }}
        >
            <NavItem href="/">Home</NavItem>
        </Navbar>
    );
}
