import React, { useState } from "react";
import "./postlist.scss";
import ModalForm from "../ModalForm";
import { httpDelete } from '../../adapters/core/http'

export default function PostList({ posts, setReloadPosts, setErrors }) {
    const [modalVisible, setModalVisible] = useState(false);
    const [modalTitle, setModalTitle] = useState("");
    const [modalContent, setModalContent] = useState({title:'', body:''});

    const modalCreate = () => {
        setModalContent({title:'', body:''})
        setModalTitle("Crear Post");
        setModalVisible(true);
    };

    const modalEdit = (post) => {
        setModalContent(post)
        setModalTitle("Editar Post");
        setModalVisible(true);
    };

    const deletePost = async (postId) => {
        const resp = httpDelete(`/posts/${postId}`);
        if(resp) {
            setReloadPosts(true);
            setErrors({msj: "El post fue eliminado correctamente"});
            return
        }
        setErrors({err: "La eliminacion del post fallo"});
    }

    return (
        <div className="wrapper">
            <ModalForm
                visible={modalVisible}
                title={modalTitle}
                isVisible={setModalVisible}
                modalContent={modalContent}
                setReloadPosts={setReloadPosts}
                setErrors={setErrors}
            />
            <button
                className="btn waves-effect waves-light btn-create"
                type="button"
                name="action"
                onClick={() => modalCreate()}
            >
                Crear Post
                <i className="material-icons right">add</i>
            </button>
            <div className="post-list">
                <ul className="collection">
                    {posts.map((post, index) => (
                        <li className="collection-item" key={index}>
                            <div className="item">
                                <div className="post-item">
                                    <a href={`post/${post.id}`}>{post.title}</a>
                                </div>
                                <div className="buttons">
                                    <div className="post-button-up">
                                        <button
                                            className="btn waves-effect waves-light"
                                            onClick={() => {
                                                modalEdit(post);
                                            }}
                                        >
                                            <i className="material-icons">
                                                create
                                            </i>
                                        </button>
                                    </div>
                                    <div className="post-button-del">
                                        <button 
                                            className="btn waves-effect waves-light red"
                                            onClick={() => deletePost(post.id)}    
                                        >
                                            <i className="material-icons">
                                                delete
                                            </i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </li>
                    ))}
                </ul>
            </div>
        </div>
    );
}
