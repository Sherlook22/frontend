import React from "react";
import { Modal, Button } from "react-materialize";
import PostForm from "../PostFrom";

export default function ModalForm(props) {
    const {
        visible,
        title,
        isVisible,
        modalContent,
        setReloadPosts,
        setErrors
    } = props;

    function openClose() {
        const form = document.getElementById('form-modal')
        form.reset();
        isVisible(false);
    }

    return (
        <Modal
            actions={[
                <Button
                    flat
                    modal="close"
                    node="button"
                    waves="green"
                    onClick={() => openClose()}
                >
                    Close
                </Button>,
            ]}
            bottomSheet={false}
            fixedFooter={false}
            header={title}
            id="Modal-0"
            open={visible}
            options={{
                dismissible: false,
                endingTop: "10%",
                inDuration: 250,
                onCloseEnd: null,
                onCloseStart: null,
                onOpenEnd: null,
                onOpenStart: null,
                opacity: 0.5,
                outDuration: 250,
                preventScrolling: true,
                startingTop: "4%",
            }}
        >
            <PostForm
                modalContent={modalContent}
                setReloadPosts={setReloadPosts}
                isVisible={isVisible}
                setErrors={setErrors}
            />
        </Modal>
    );
}
