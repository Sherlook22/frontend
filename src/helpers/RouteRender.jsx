import React from 'react';
import { Route } from 'react-router';

export function RouteRender(route) {
    return (
        <Route
            path={route.path}
            exact={route.exact}
            render={(props) => <route.component routes={route.routes} {...props} />}
        />
    )
}

