import Home from '../views/Home';
import PostsDetails from '../views/PostDetails';

const routes = [
    {
        path: "/",
        component: Home,
        exact: true
    },
    {
        path: "/post/:id",
        exact: true,
        component: PostsDetails
    }
]

export default routes;