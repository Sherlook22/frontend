import axios from './axios';

async function httpGet(url) {
    try {
        const resp = await axios.get(url);
        return resp.data;
    } catch (e) {
        return null;
    }
}

async function httpPost(url, obj) {
    try {
        const resp = await axios.post(url, obj);
        return resp;
    }catch(e) {
        return null;
    }
}

async function httpPatch(url, obj) {
    try {
        const resp = await axios.patch(url, obj);
        return resp;
    }catch(e) {
        return null;
    }
}
async function httpDelete(url, obj) {
    try {
        const resp = await axios.delete(url, obj);
        return resp;
    }catch(e) {
        return null;
    }
}

export {
    httpGet,
    httpPost,
    httpPatch,
    httpDelete
}
