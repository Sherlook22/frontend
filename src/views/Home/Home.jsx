import React, {useEffect, useState } from 'react';
import './home.scss';
import { httpGet } from '../../adapters/core/http'
import PostList from '../../components/PostList';

export default function Home() {
  const [posts, setPosts] = useState([]);
  const [errors, setErrors] = useState({err:'', msj:''});
  const [reloadPosts, setReloadPosts] = useState(false);

  useEffect(() => {
    async function getPostList() {
      const posts = await httpGet('/posts');
      setPosts(posts);
      setReloadPosts(false);
      if(errors) {
        setTimeout(() => {
          setErrors({err:'', msj:''});
        },3000)
      }
    }
    getPostList();
  }, [reloadPosts]);

  return (
    <>
        <p className={errors.err ? "p-error" : "p-succes"}>{errors.err ? errors.err : errors.msj}</p>
        <div className="container">
          <h1>Los mejores POST</h1>
          <PostList posts={posts} setReloadPosts={setReloadPosts} setErrors={setErrors} />
        </div>
    </>
  );
}

