import React, {useEffect, useState} from "react";
import "./postdetails.scss";
import { useParams } from "react-router-dom";
import { httpGet } from '../../adapters/core/http';

export default function PostsDetails() {
    const {id} = useParams();
    const [post, setPost] = useState({})
    let resp = '';

    useEffect(() => {
        const getPost = async () => {
            const post = await httpGet(`/posts/${id}`);
            if(post) setPost(post)
            else setPost(null)
        }
        getPost();        
        
    }, [id]);
    
    if(post !== null) {
        resp = <Details post={post} />
    }

    return (
        <div className="row details">
            <div className="col s12 m12">
                {resp ? resp : <h2>No existen post con el id referenciado</h2>}
            </div>
        </div>
    );
}

function Details({post}) {
    return (
        <div className="card blue-grey darken-3">
            <div className="card-content white-text">
                <span className="card-title">{post.title}</span>
                <p>
                    {post.body}
                </p>
            </div>
            <div className="card-action">
                <a href="/">Home</a>
            </div>
        </div>
    )
}
